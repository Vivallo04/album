#ifndef ALBUM_SONG_H
#define ALBUM_SONG_H

#include "cstring"
#include <iostream>
#include <bits/stdc++.h>

//TODO: ✓ titulo: una variable String que guarda el título de la canción.
//      ✓ autor: una variable String que guarda el autor de la canción. y los siguientes
//  métodos:
//      *------------------ GETTERS & SETTERS -----------------*
//      ✓ Cancion(String, String): constructor que recibe como parámetros el título y el autor de la canción (por este orden).
//      ✓ Cancion(): constructor predeterminado que inicializa el título y el autor a cadenas vacías.
//      ✓ dameTitulo(): devuelve el título de la canción.
//      ✓ dameAutor(): devuelve el autor de la canción.
//      ✓ ponTitulo(String): establece el título de la canción.
//      ✓ ponAutor(String): establece el autor de la canción.

class Song
{
public:
    Song *previous;
    Song *next;

    // Constructors
    Song(std::string title, std::string author);
    Song();

    // Getters
    std::string GetTitle();
    std::string GetAuthor();
    int GetYear();

    // Setters
    void SetTitle(std::string title);
    void SetAuthor(std::string author);
    void SetYear(int year);
    void AddSongToFront(Song **head);
    void AddSongToTail(Song **head, std::string title, std::string author);
    void PrintAlbum(Song *head);
    void PrintBackwards(Song *head);

private:
    std::string title;
    std::string author;
    int year;
};


#endif //ALBUM_SONG_H
