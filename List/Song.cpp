#include "Song.h"
#include <bits/stdc++.h>


Song::Song(std::string title, std::string author)
{
    this -> title = title;
    this -> author = author;
    this -> year = 2014;
}


Song::Song()
{
    this -> title = "";
    this -> author = "";
    this -> year = 0;
}


//<return type> Class::MethodName(params...){}
std::string Song::GetTitle()
{
    return std::string();
}


std::string Song::GetAuthor()
{
    return std::string();
}


int Song::GetYear()
{
    return 0;
}


void Song::SetTitle(std::string title)
{

}


void Song::SetAuthor(std::string author)
{

}


void Song::SetYear(int year)
{

}


void Song::AddSongToFront(Song **head)
{
    Song *new_song = new Song();

    new_song -> next = *head;
    new_song -> previous = NULL;

    if ((*head) != NULL)
    {
        (*head) -> previous = new_song;
    }
    (*head) = new_song;
}


void Song::AddSongToTail(Song **head, std::string title, std::string author)
{
    Song *new_song = new Song(title, author);
    Song *last = *head;

    new_song -> next = NULL;

    if (*head ==  NULL)
    {
        new_song -> previous = NULL;
        *head = new_song;
        std::cout << new_song -> title << std::endl;
        return;
    }

    while (last -> next != NULL)
    {
        last = last -> next;
    }
    last -> next = new_song;
    new_song -> previous = last;
}


void Song::PrintAlbum(Song *head)
{
    Song *last;
    while (head != NULL)
    {
        std::cout << head -> title << std::endl;
        last = head;
        head = head -> next;
    }
    std::cout << "" << std::endl;
}


void Song::PrintBackwards(Song *head)
{
    Song *last;

    while (last != NULL)
    {
        std::cout << last -> title << std::endl;
        last = last -> previous;
    }
    std::cout << " " << std::endl;
}




